require 'spec_helper'

require 'gitlab/triage/resource/milestone'

describe Gitlab::Triage::Resource::Milestone do
  include_context 'network'

  let(:resource) do
    {
      id: 1,
      iid: 11,
      project_id: 111,
      title: 'title',
      description: 'description',
      state: state,
      due_date: due_date.iso8601,
      start_date: start_date.iso8601,
      updated_at: updated_at.iso8601,
      created_at: created_at.iso8601
    }
  end

  let(:state) { 'active' }
  let(:due_date) { Date.new(2018, 2, 1) }
  let(:start_date) { Date.new(2018, 1, 1) }
  let(:updated_at) { Time.new(2017, 2, 1) }
  let(:created_at) { Time.new(2017, 1, 1) }

  subject { described_class.new(resource, network: network) }

  it_behaves_like 'resource fields'

  describe '#succ' do
    let(:milestones) do
      [
        {
          id: 0,
          project_id: 111,
          start_date: start_date.iso8601
        },
        {
          id: 1,
          project_id: 111,
          start_date: start_date.next_month.iso8601
        },
        {
          id: 2,
          project_id: 111,
          start_date: start_date.next_day.iso8601
        },
        {
          id: 3,
          project_id: 111
        }
      ]
    end

    before do
      stub_api(
        :get,
        "http://test.com/api/v4/projects/#{resource[:project_id]}/milestones",
        query: { per_page: 100, state: 'active' }) { milestones }
    end

    context 'when current milestone is the 1st one' do
      let(:resource) { milestones[0] }

      it 'returns the next active milestone' do
        succ = subject.succ

        expect(succ).to be_kind_of(described_class)
        expect(succ.id).to eq(milestones.dig(2, :id))
      end
    end

    context 'when current milestone is the 2nd one' do
      let(:resource) { milestones[1] }

      it 'returns nil because there is no next milestone' do
        succ = subject.succ

        expect(succ).to be_nil
      end
    end

    context 'when current milestone is the 3rd one' do
      let(:resource) { milestones[2] }

      it 'returns the next active milestone' do
        succ = subject.succ

        expect(succ).to be_kind_of(described_class)
        expect(succ.id).to eq(milestones.dig(1, :id))
      end
    end

    context 'when current milestone is the 4th one' do
      let(:resource) { milestones[3] }

      it 'returns nil because there is no next milestone' do
        succ = subject.succ

        expect(succ).to be_nil
      end
    end
  end

  describe '#active?' do
    context 'when state is active' do
      let(:state) { 'active' }

      it 'returns true' do
        expect(subject).to be_active
      end
    end

    context 'when state is closed' do
      let(:state) { 'closed' }

      it 'returns true' do
        expect(subject).not_to be_active
      end
    end
  end
end
