# frozen_string_literal: true

require 'spec_helper'

describe 'filter with ruby milestone' do
  include_context 'integration'

  let(:issue_with_closed_milestone) do
    issue.merge(milestone: issue[:milestone].merge(state: 'closed'))
  end

  let(:issue_with_active_milestone) do
    issue.merge(milestone: issue[:milestone].merge(state: 'active'))
  end

  let(:issues) { [issue_with_closed_milestone, issue_with_active_milestone] }

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  it 'comments on the issue with closed milestone' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Nudge on issues with closed milestone
              conditions:
                ruby: |
                  !milestone.active?
              actions:
                comment: |
                  The milestone %"\#{milestone.title}" has been closed!
    YAML

    iid = issue_with_closed_milestone[:iid]

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{iid}/notes",
      body: { body: 'The milestone %"v4.0" has been closed!' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end
end
